#!/bin/sh
set -e

if [ -e /.installed ]; then
  echo 'Already installed.'

else
  echo ''
  echo 'INSTALLING'
  echo '----------'
  
  echo "-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEAv5jbJHYNRYT5jYGJL81xhAUthmjljCUSDEftrw9/9K8pEOth
2tZ/OTKxYP6wsg6vhJsLdncgcm7m0WFbWQJ2IFM3fVhUBHmEOMON//DTImx37CpO
y4b7Icl/BbJHDoIQt62f9DdAbjykK5f/UKFf6iKI7XV5zvSOaKmjIrhgtvheEx+z
wqnn92YiDxmdJLAhzh6HdRkOC7u04iOTsr8i3kfrGgaa+ykxXUDcXGmJGzmUWj5t
5wpOHiF7QB+FtML7/wJCrQBLAIQ1wiNn1bHcBWIQ8eDW1Q3qQHJzCBCfcXk6Fnud
HLoAqXOZ1+X6U4O6cPy6HdMtLEuHADZoZZLdlQIDAQABAoIBABKULPPdKq5KFbTe
EcJ75n0Jz39QHqo3Uge01dx64UqFzbKhUIza7d1LCB5CtDmf6O8v5a2oIZJXJLS6
/xOjhHbf+uqBrSv96I9kjxl5bc/RutNAywE7ZpDBLDsOLYu/mwVkpbUVwn6uDLFZ
fN2BuH5IuQRlttkVp9WIVZRhUqeLRMqstSCYULfG8IUzxbCwXzB2fykRahy8vIfF
6cRh3KdcdPAU3WZN8ZTQr2r4ahZcHBTTaVNyHafNcpKwtgZQLXFkBNgcfKqgMcfh
CucXcSCFGG0k/tCjOlG7IHNlUbzTXrkT+QRRHgQW7TWtp/1xuxickS8E+5u/vrKt
Q/SgVWUCgYEA9jhJ0jx59TK/zNcdP2eml2+PjS2COqfUgNxxkRGWrCUnNap0+D/7
V/gszVjhspcrBnK8kdlAAfWThlV9e0u6Au9guL9eheYzMy/d773t+EIyMYTfH8JN
SirIaW7qwhiNeGQa5zPzfiKJmseR7KnbZFG0Dl1rjdpqJhmq1LrYgyMCgYEAxzUh
YTW5TTNIBJDhDvdTGkSMVYnpmb0i+QbtH9KuXSJiKC5bt6GgLXnF8KdzK5pn9yBy
qqwgN7KkLYyZtqkTx1QiWumXGpRMuUC+xU92iX0/D8vbU6tDWJ+yaLnoTnT0J1qI
Q9DC3vf9zbuT6mks4wTpcxb3fSFTKSGut/dqY+cCgYAxHV9Uaa3mRH5QDSUPbcyk
bqOTBN3yd2IDZnXc+0Y0BqFAqkT09EZd8Op29+DlsBPsEF0fNtf2zBbmeA002BE9
3qRLlhjz2syhzE2YMinTGNlMWXXsruSeA9TAe1im6SDnQAjBz/IlWhxa3K52KBfU
dhvVtbrrrAL6oKocDz17QwKBgAr8NREFQ2iscUY/LwoCPYTvTsXQyx79PRZ6eU0N
8yuTn9ITbF9d/FVFp9KziThYVW9dTLV4tHYd1cvlOvzFqh+N1i8pPnnqv6hfl6WN
0j2WuImIdlnQ0e/rLo1z4wmgjW42IdmLg5OEHWzg8TE2B5GlTbJZC+DMm6djAh2G
+Uj1AoGABwAb2iHLrScWjPP3vUI+o7avZerbqjDUfbEsAva/q7QraLNk1AJ1pJBE
IR19Ww4xwBEwAW4bDsGpF1ShyyT6JKI3dxw8nTNvbCWRzIcGEpTLhLa94RqXNxhx
HgPnpWs0vrqGVyQPlIlmms4YzoNlZ3NoMALAgOAe0bMsQTxlFDY=
-----END RSA PRIVATE KEY-----" >> /home/vagrant/mykey.pem

chmod 600 /home/vagrant/mykey.pem
chown vagrant:vagrant /home/vagrant/mykey.pem
#ssh -i /home/vagrant/mykey.pem vagrant@192.168.100.2 -o StrictHostKeyChecking=no "sudo /sbin/ifconfig eth0 down"
#ssh -i /home/vagrant/mykey.pem vagrant@192.168.100.2 -o StrictHostKeyChecking=no "sudo /sbin/ifconfig"
#ssh -i /home/vagrant/mykey.pem vagrant@192.168.100.3 -o StrictHostKeyChecking=no "sudo /sbin/ifconfig eth0 down"
#ssh -i /home/vagrant/mykey.pem vagrant@192.168.100.3 -o StrictHostKeyChecking=no "sudo /sbin/ifconfig"
  
  ##The rest goes here
fi

sudo echo 1 > /proc/sys/net/ipv4/ip_forward
sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
sudo iptables -A FORWARD -i eth0 -o eth1 -m state --state RELATED,ESTABLISHED -j ACCEPT
sudo iptables -A FORWARD -i eth0 -o eth2 -m state --state RELATED,ESTABLISHED -j ACCEPT
sudo iptables -A FORWARD -i eth1 -o eth0 -j ACCEPT
sudo iptables -A FORWARD -i eth2 -o eth0 -j ACCEPT