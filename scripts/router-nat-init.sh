#!/bin/sh
echo "Installing and configuring DHCP"

sudo apt-get update
sudo apt-get install isc-dhcp-server

cd /home/vagrant

if [ -e /etc/default/isc-dhcp-server ]; then
        sudo rm /etc/default/isc-dhcp-server
fi

echo 'INTERFACES="eth1"' > isc-dhcp-server
sudo \cp isc-dhcp-server /etc/default/isc-dhcp-server

sudo rm /etc/dhcp/dhcpd.conf
if [ -e dhcpd.conf ]; then
        rm dhcpd.conf
        touch dhcpd.conf
fi
echo -e 'default-lease-time 600;' >> dhcpd.conf
echo -e 'max-lease-time 7200;' >> dhcpd.conf
echo -e 'subnet 192.168.100.0 netmask 255.255.255.0 {' >> dhcpd.conf
echo -e 'range 192.168.100.10 192.168.100.100;' >> dhcpd.conf
echo -e 'option routers 192.168.100.1;' >> dhcpd.conf
echo -e 'option subnet-mask 255.255.255.0;' >> dhcpd.conf
echo -e 'option broadcast-address 192.168.100.255;' >> dhcpd.conf
echo -e 'option domain-name-servers 8.8.8.8, 192.168.100.1;' >> dhcpd.conf
echo -e 'option ntp-servers 192.168.100.1;}' >> dhcpd.conf

if [ -e /etc/dhcp/dhcpd.conf ]; then
        sudo rm /etc/dhcp/dhcpd.conf
fi

sudo \cp dhcpd.conf /etc/dhcp/dhcpd.conf

sudo service isc-dhcp-server restart



sudo echo 1 > /proc/sys/net/ipv4/ip_forward

sudo iptables -F
sudo iptables -X
sudo iptables -t nat -F
sudo iptables -t nat -X
sudo iptables -t mangle -F
sudo iptables -t mangle -X
sudo iptables -P INPUT ACCEPT
sudo iptables -P OUTPUT ACCEPT
sudo iptables -P FORWARD ACCEPT

sudo iptables -t nat -A POSTROUTING -o eth2 -j MASQUERADE
sudo iptables -A FORWARD -i eth2 -o eth1 -m state --state RELATED,ESTABLISHED -j ACCEPT
sudo iptables -A FORWARD -i eth1 -o eth -j ACCEPT